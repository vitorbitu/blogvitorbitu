---
title: "Chile"
imagem: "http://sweetway.com.br/wp-content/uploads/2019/01/tudo-o-que-voce-precisa-saber-sobre-santiago-no-chile.jpg"
date: 2022-10-09T21:42:53-03:00
draft: false
---

Minha viagem para o Chile em janeiro de 2019.
Saindo de guarulhos para Santiago, mas dessa vez com a família incompleta pois minhas irmãs não puderam ir, então só foi eu, meu irmão e meus pais. Foi uma viagem muito divertida, conhecemos o maior prédio da América Latina, comemos pizza em um prédio que gira e conhecemos as termas de santiago, entre outras coisas. Andamos bastante pela capital do Chile, Santiago, ficamos por lá por duas semanas, e pela primeira vez tivemos a experiência de vivenciar um terremoto, foi bem leve, mas foi um episódio que ficará marcado para sempre. Demos muita risada, porque meu pai de todos foi o que mais teve medo e desceu correndo as escadas chegando no térreo branco de susto.