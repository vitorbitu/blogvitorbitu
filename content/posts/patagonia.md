---
title: "Patagonia"
imagem: "https://www.submarinoviagens.com.br/bora-nessa-trip/wp-content/uploads/2019/12/patagonia-perito-moreno-shutterstock-649655275.jpg"
date: 2022-10-08T14:43:59-03:00
draft: false
---

Minha viagem para Patagonia em janeiro de 2017.
A viagem começa no dia 20 de janeiro de 2017, enquanto eu tinha 15 anos de idade. Mal podia esperar pelo tanto de coisas interessantes que iria conhecer nessa viagem, geleiras, lagos paradisíacos e cidades maravilhosas. O dia mais interessante foi no primeiro dia que fomos conhecer as geleiras, pois eu não tinha ideia de quão grande eram aquelas paredes de gelo. Em um passeio de barco passamos a menos de 500m de distância da geleira, e enquanto passávamos ali o guia nos disse que elas tem em média 70m de altura desde a água do mar, até o topo do gelo. 
Já nos próximos dias fizemos passeios de trilha a pé pelas montanhas, onde a recompensa no final era conhecer os lagos azuis paradisíacos que ficavam entre uma montanha e outra. Parecia plano de fundo de celular, tudo perfeito e maravilhoso.
Visitamos também pontos de esqui, que no acaso não estavam funcionando, pois era verão naquela época. Mas a vontade de conhecer novamente o local no inverno para poder esquiar na neve era muito grande. 
Depois de dias viajando e conhecendo o fim do mundo (forma que as pessoas chamam a região do ushuaia), voltamos a São Paulo com uma experiência única e que lembrarei para sempre.