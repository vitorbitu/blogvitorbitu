---
title: "Italia"
imagem: "https://h8f7z4t2.stackpathcdn.com/wp-content/uploads/2019/12/coliseu-roma.jpg"
date: 2022-10-08T14:43:18-03:00
draft: false
---

Minha viagem para Itália em setembro de 2022
A viagem começa no dia 10 de setembro de 2022, saindo de Guarulhos e chegando no aeroporto de Roma, onde encontramos a minha irmã Sara, que mora em Portugal. 
A partir daí seguimos em direção a Siena, onde conhecemos Bagnoreggio no caminho e Montepulciano. Passamos um dia em Siena e depois no caminho para Florença passamos por San Gimignano e Bologna, onde experimentamos a verdadeira bolonhesa italiana. Em florença, passamos 3 dias e conhecemos a estátua de David, e outros museus e catedrais. Depois fomos a veneza, onde passamos 2 dias e conhecemos muitos lugares maravilhosos e Murano, ponto de fábricas e museus de vidro. O próximo destino foi Sirmione, onde passamos um dia e conhecemos os arredores do lago de garda. O dia seguinte amanhacemos em Sirmione e já partimos para Roma, onde ficamos 4 dias e conhecemos seus maiores pontos turísticos: Coliseu, Ruínas, Vaticano, Panteão e a Fontana di Trevi. No dia 23 de setembro voltamos do aeroporto de Roma para Guarulhos, encerrando essa incrível viagem em família.