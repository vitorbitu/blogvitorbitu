---
subtitulo: "Este é o meu blog pessoal"
---

Neste site, você encontrará

- Minha biografia

- Postagens sobre duas viagens que realizei com toda a minha família (minhas duas irmãs, meu irmão e meus pais). Meus pais sempre fazem de tudo para que a gente continue sempre unidos, e por mais que a correria da vida dificulte muito essas viagens, eles sempre fazem acontecer. Isto é algo que tenho muito orgulho e quero passar para os meus filhos quando for a hora. Família é um bem maior e fica ainda mais especial quando podemos juntar com as viagens. 