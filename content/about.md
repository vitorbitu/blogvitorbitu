---
title: "Biografia"
imagem: "../images/biografia.jpg"
date: 2022-10-08T14:20:22-03:00
draft: false
---

Vitor Vac Bitu Alves, nascido em 11 de julho de 2001 em São Paulo, SP. Filho de Cristina Helena Vac Bitu e Douglas Bitu Alves. 
No âmbito profissional:
Estudou na escola primária Mundo Mágico em Cotia, SP até antes do primeiro ano do fundamental, quando foi para o Colégio Objetivo Granja Viana, onde estudou do primeiro ano do fundamental até a metade do primeiro ano do Ensino Médio. Logo em seguida, Vitor fez um intercâmbio de um ano letivo em Minnesota, EUA, na cidade de Hendrum. Lá ele praticou esportes como: basquete, futebol americano e atletismo e ganhou muita experiência. No meio de 2018 Vitor voltou ao Brasil para terminar o ensino médio no Colégio Objetivo da Paz.
Logo que se formou, foi aceito na Escola Politécnica da Universidade de São Paulo, onde está cursando sua graduação de Engenharia Mecatrônica. 

Nos quesitos sociais:
Vitor sempre teve muitos amigos nas escolas por onde estudou, e a maior parte de seus amigos mais próximos são da Granja Viana, onde ele morou pela maior parte de sua vida. Foi lá também em 2016 que ele conheceu sua atual namorada, Thipphane.
Foi em 2018 que o namoro começou e esteve firme e forte desde então, por mais de 4 anos.